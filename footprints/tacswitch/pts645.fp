Element[ "" "Created by footgen" "??" "none" 1000mil 1000mil 5.35mm -0.75mm 0 100 "" ]
(
Pad [ -4.100mm 2.250mm -3.850mm 2.250mm 1.2999999999999998mm 0.500mm 1.452mm "1" "1" "square"]
Pad [ 3.850mm 2.250mm 4.100mm 2.250mm 1.2999999999999998mm 0.500mm 1.452mm "2" "2" "square"]
Pad [ 3.850mm -2.250mm 4.100mm -2.250mm 1.2999999999999998mm 0.500mm 1.452mm "3" "3" "square"]
Pad [ -4.100mm -2.250mm -3.850mm -2.250mm 1.2999999999999998mm 0.500mm 1.452mm "4" "4" "square"]
ElementLine[ -5.250mm 3.400mm -5.250mm -3.400mm 0.127mm ]
ElementLine[ -5.250mm -3.400mm 5.000mm -3.400mm 0.127mm ]
ElementLine[ 5.000mm -3.400mm 5.000mm 3.400mm 0.127mm ]
ElementLine[ 5.000mm 3.400mm -5.250mm 3.400mm 0.127mm ]
ElementArc[ -5.750mm 2.250mm 0.125mm 0.125mm 0.000 359.000 0.250mm ]
)
