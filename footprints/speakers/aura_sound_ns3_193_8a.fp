# Hey Emacs, use -*- Tcl -*- mode
Element["" "" "" "" 3.4396mm 3.4396mm 0.0000 0.0000 0 100 ""]
(
 # Center hole should be 3.000 inches in diameter.  It should be a
 # non-plated (hole), and thus should have no copper annulus.  The
 # thickness should be the same size as the drill diameter.
 #
 # Mouting holes are for 6-32 surface-mount PEMs -- 0.213 drill and
 # 0.306 solder pad width.
 #
 # Pin[x y thickness clearance mask drill name number flags]
	Pin[0.0mil 0.0mil 3000.0mil 20mil 3006.0mil 3000.0mil "" "1" "hole"]
	Pin[1295.0mil 1295.0mil 313.0mil 20mil 319.0mil 213.0mil "" "2" ""]
	Pin[-1295.0mil 1295.0mil 313.0mil 20mil 319.0mil 213.0mil "" "3" ""]
	Pin[-1295.0mil -1295.0mil 313.0mil 20mil 319.0mil 213.0mil "" "4" ""]
	Pin[1295.0mil -1295.0mil 313.0mil 20mil 319.0mil 213.0mil "" "5" ""]
	)






