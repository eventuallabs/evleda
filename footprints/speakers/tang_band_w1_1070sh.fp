# Hey Emacs, use -*- Tcl -*- mode
Element["" "" "" "" 3.4396mm 3.4396mm 0.0000 0.0000 0 100 ""]
(
 # Center hole should be 1.625 inches in diameter.  It should be
 # non-plated (hole), and thus should have no copper annulus.  The
 # thickness should be the same size as the drill diameter.
	Pin[0.0mil 0.0mil 1625.0mil 20mil 1631.0mil 1625.0mil "" "1" "hole"]
	Pin[668.0mil 668.0mil 244.0mil 20mil 250.0mil 166.0mil "" "2" ""]
	Pin[-668.0mil 668.0mil 244.0mil 20mil 250.0mil 166.0mil "" "3" ""]
	Pin[-668.0mil -668.0mil 244.0mil 20mil 250.0mil 166.0mil "" "4" ""]
	Pin[668.0mil -668.0mil 244.0mil 20mil 250.0mil 166.0mil "" "5" ""]
	ElementLine[-668mil -846mil 668mil -846mil 10mil]
	ElementLine[-668mil 846mil 668mil 846mil 10mil]
	ElementLine[-846mil 668mil -846mil -668mil 10mil]
	ElementLine[846mil 668mil 846mil -668mil 10mil]
	)



