Element[ "" "Created by footgen" "??" "none" 1000mil 1000mil 1.95mm -1.55mm 0 100 "" ]
(
Pad [ -0.950mm 1.050mm -0.950mm 1.550mm 0.6mm 0.500mm 0.752mm "1" "1" "square"]
Pad [ 0.000mm 1.050mm 0.000mm 1.550mm 0.6mm 0.500mm 0.752mm "2" "2" "square"]
Pad [ 0.950mm 1.050mm 0.950mm 1.550mm 0.6mm 0.500mm 0.752mm "3" "3" "square"]
Pad [ 0.950mm -1.050mm 0.950mm -1.550mm 0.6mm 0.500mm 0.752mm "4" "4" "square"]
Pad [ 0.000mm -1.050mm 0.000mm -1.550mm 0.6mm 0.500mm 0.752mm "5" "5" "square"]
Pad [ -0.950mm -1.050mm -0.950mm -1.550mm 0.6mm 0.500mm 0.752mm "6" "6" "square"]
ElementLine[ -1.750mm 2.350mm -1.750mm -2.350mm 0.127mm ]
ElementLine[ -1.750mm -2.350mm 1.750mm -2.350mm 0.127mm ]
ElementLine[ 1.750mm -2.350mm 1.750mm 2.350mm 0.127mm ]
ElementLine[ 1.750mm 2.350mm -1.750mm 2.350mm 0.127mm ]
ElementArc[ -2.250mm 1.550mm 0.125mm 0.125mm 0.000 359.000 0.250mm ]
)
