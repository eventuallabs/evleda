Element[ "" "Created by footgen" "??" "none" 1000mil 1000mil 1.65mm -1.25mm 0 100 "" ]
(
Pad [ -0.650mm 0.950mm -0.650mm 1.250mm 0.6mm 0.500mm 0.752mm "1" "1" "square"]
Pad [ 0.650mm 0.950mm 0.650mm 1.250mm 0.6mm 0.500mm 0.752mm "2" "2" "square"]
Pad [ 0.000mm -0.950mm 0.000mm -1.250mm 0.6mm 0.500mm 0.752mm "3" "3" "square"]
ElementLine[ -1.075mm 0.650mm -1.075mm -0.650mm 0.127mm ]
ElementLine[ -1.075mm -0.650mm -0.500mm -0.650mm 0.127mm ]
ElementLine[ 1.075mm 0.650mm 1.075mm -0.650mm 0.127mm ]
ElementLine[ 1.075mm -0.650mm 0.500mm -0.650mm 0.127mm ]
)
