# Hey Emacs, use -*- Tcl -*- mode
Element["" "" "" "" 3.4396mm 3.4396mm 0.0000 0.0000 0 100 ""]
(
 # Pin[x y thickness clearance mask drill name number flags]
 #
 # The center knob hole
 Pin[0.0mil 0.0mil 410.0mil 20mil 416.0mil 390.0mil "" "1" ""]
 # The alignment hole
 Pin[375.0mil 0.0mil 145.0mil 20mil 151.0mil 125.0mil "" "1" ""]

 # ElementArc [x y r1 r2 startangle sweepangle thickness]
 #
 # The knob will be 1 inch in diameter, so make that the keepout
 ElementArc [0 0 500mil 500mil 0 359 10mil]
 

)


